# restator

restator pour leekwars.com.
Leekwars est un jeu où on doit programmer un poireau pour affronter d'autres poireaux en arène.
Le restator permet de faire des simulations pour réattribuer le capital d'un poireau.



# Requis

Il vous faut un navigateur web récent acceptant les scripts js. Tout se passe sur votre machine. Le serveur ne fait qu'envoyer les fichiers. Vous pouvez donc l'utiliser hors-ligne.

A noter que le projet n'a été conçu et testé que sur Firefox. Il a aussi été testé sur chromium. Pour le reste, je n'en sais rien...


# Installation

Téléchargez simplement les fichiers dans un dossier et ouvrez restator.html avec votre navigateur préféré.


# Utilisation

Vous avez des boutons ±1, ±10, ±100 pour toutes les caractéristiques. Elles s'adaptent si besoin.

Ensuite, vous avez trois onglets pour les armes et les puces.
 * onglet «équipement poireau»: ce sont les armes et puces qui sont actuellement sur le poireau simulé.
 * onglet «armes»: regroupe l'ensemble de toutes les armes accéssibles au niveau du poireau simulé.
 Il permet aussi de sélectionner les armes pour équipement.
 * onglet «puces»: regroupe l'ensemble des puces accéssibles au niveau du poireau simulé, groupées par catégories.
 On peut sélectionner les puces pour équipement.
 
Les effets des armes s'adaptent automatiquement en fonction des caractéristiques choisies pour le poireau.
Bien sûr, les effets ne tiennent pas compte de la cible: S'il est indiqué 120 de dégâts,
ça ne prend pas en compte les éventuels boucliers de la cible.

Certaines cases ont des informations supplémentaires sur les effets. On y accède en survolant la case avec la souris : une info-bulle apparaît.


Il y a aussi un bouton d'import/export qui permet d'importer un poireau à partir de son identifiant sur LeekWars.
On peut aussi sauvegarder sa simulation: un code est fourni dans la fenêtre d'import/export. Il s'agit d'une suite de lettres.
Elle est à copier/coller.
Il suffira alors de rappeler ce code dans le champs d'import pour retrouver sa simulation.


# Bugs et améliorations

Il y en a sûrement ! MP-moi dans le jeu ou envoyez un ticket ici.
